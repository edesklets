#! /bin/sh
#
# Invoke the autotools...
#
# ./autogen.sh			Should create the ./configure script
# ./autogen.sh --clean  	Should purge the whole thing
#
# The cleaning part is more complex than it should, but at least it's 
# portable.
#
GETTEXT_H=/usr/share/gettext/gettext.h
PRESERVE=`(cat <<EOF
COPYING
acinclude.m4
autogen.sh
configure.in
Makefile.am
src
artwork
pot
EOF
)`

SEDFILTER=`echo "${PRESERVE}" | sed 's/.*/\/^&$\/d/' | sed 'N;s/\n/;/'`
for F in `ls | sed "${SEDFILTER}"` ; do
    echo "Removing '${F}'..."
    rm -r ${F}
done

test "x$1" = "x--clean" && exit 0

for F in README NEWS AUTHORS ChangeLog ; do 
    echo "Creating empty '${F}'..."
    echo "All details available on http://edesklets.sf.net/" > ${F}
done
sed -i 's/po\/Makefile.in//' configure.in
sed -i 's/config.rpath//g;s/po//g' Makefile.am
test -e ${GETTEXT_H} && cp ${GETTEXT_H} src/

set -x 
gettextize --force --copy --no-changelog
(
cd po
for F in `ls ../pot/*` ; do
    ln -f -s ${F}
done
)    
aclocal -I m4
libtoolize --force
autoheader
automake --add-missing
autoconf
