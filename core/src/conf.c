/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

#ifdef HAVE_STRING_H
#include <string.h>	/* memcpy() */
#endif

/*---------------------------------------------------------------------------*/
const Desklet DESKLET_DEFAULTS = {
  NULL,                 /* gchar * id;                  */
  NULL, 		/* gchar * uri;  		*/
  0, 0, 300, 200,   	/* gint x, y, width, height; 	*/
  FALSE, 		/* gboolean decorate; 		*/
  5, 1 			/* gint timeout, attempts; 	*/
};

/*---------------------------------------------------------------------------*/
void
config_default(Desklet * desc) {
  g_memmove(desc, &DESKLET_DEFAULTS, sizeof(Desklet));
}

/*---------------------------------------------------------------------------*/
gchar *
config_path(gchar * basename) {
  gchar * path, * ret;

  path = g_build_filename(g_get_user_config_dir(), PACKAGE_NAME, NULL);
  if (!g_file_test(path, G_FILE_TEST_EXISTS)) {
    g_warning(_("trying to create configure dir '%s'"), path);
    g_mkdir_with_parents(path, 0755);
  }
  ret = g_build_filename(path, basename, NULL);
  g_free(path);
  return ret;
}

/*---------------------------------------------------------------------------*/
static EConfig * 
config_new(void) {
  EConfig * conf;
  if ((conf = g_malloc0(sizeof(EConfig))))
    if (!(conf->desklets = g_array_new(FALSE, TRUE, sizeof(Desklet)))) {
      g_free(conf);
      conf = NULL;
    }
  return conf;
}

/*---------------------------------------------------------------------------*/
EConfig * 
config_load(gboolean nolock, gboolean readonly) {
  GError * err = NULL;
  GIOChannel * gio;
  gchar * fn, * buf, ** groups;
  gint i;
  gsize n;
  Desklet desc;
  EConfig * conf;
  
#define crash(...) \
do{\
  conf->crashmode = TRUE;\
  conf->crashmsg = g_strdup_printf(__VA_ARGS__);\
  g_warning(conf->crashmsg);\
}while(0)

  if ((conf = config_new())) {
    conf->kf = g_key_file_new();
    fn = config_path("config");
    if ((conf->lf = fopen_locked(fn, 
				 g_file_test(fn, G_FILE_TEST_EXISTS)?"r+":"w+", 
				 !nolock, FALSE || readonly, &err))) {
      gio = g_io_channel_unix_new(fileno(file(conf->lf)));
      g_io_channel_read_to_end(gio, &buf, &n, NULL);
      g_io_channel_unref(gio);
      if (n>0) {
	if (g_key_file_load_from_data(conf->kf, buf, n, 
				      G_KEY_FILE_KEEP_COMMENTS,
				      &err)) {
	  groups = g_key_file_get_groups(conf->kf, &n);
	  for(i=0; i<n; ++i)
	    if (g_ascii_strcasecmp("general", groups[i])!=0) {
#define KEY_GET(param, type) \
do {\
  g_clear_error(&err);\
  desc.param = g_key_file_get_ ## type(conf->kf, groups[i], #param, &err);\
  if (err) {\
    g_clear_error(&err);\
    g_warning(_("could not retrieve value for %s on %s"), #param, groups[i]);\
    desc.param = DESKLET_DEFAULTS.param;\
  }\
} while(0)
	      KEY_GET(id, string);
	      KEY_GET(uri, string);
	      KEY_GET(x, integer);
	      KEY_GET(y, integer);
	      KEY_GET(width, integer);
	      KEY_GET(height, integer);
	      KEY_GET(decorate, boolean);
	      KEY_GET(timeout, integer);
	      KEY_GET(attempts, integer);
#undef KEY_GET
	      g_array_append_val(conf->desklets, desc);
	    }
	  g_strfreev(groups);
	} else {
	  g_warning(err->message);
	  crash(_("Loading settings from '%s' skipped entirely " 
		  "due to parsing error."), fn);
	  g_error_free(err);
	}
      } else
	g_warning(_("config file has zero lenght (first use?)."));
      
      g_free(buf);
    } else {
      if (err->code == EERROR_ALREADY_LOCKED) {
	g_warning(_("could not get lock on '%s': %s"), fn, err->message);
	crash(_("Could not lock config file: "
		"you can only edit one desklet at a time. "
		"If you are not already editing the config of another desklet, "
		"this might mean locking is not supported by the local system: "
		"look at eDesklets online FAQ."));
      } else {
	g_warning(err->message);
	crash(_("Could not load config file: "
		"look at eDesklets online FAQ."));
      }
      g_error_free(err);
    }
    g_free(fn);
    if ((conf->crashmode = conf->crashmode || readonly)) {
      if (conf->kf) g_key_file_free(conf->kf); conf->kf = NULL;
      if (conf->lf) fclose_locked(&conf->lf, NULL);
    }
  } else
    g_error(_("Could not allocate configuration"));
  return conf;

#undef crash
}

/*---------------------------------------------------------------------------*/
gint
config_merge(EConfig * conf, Desklet * desc) {
  gint i;
  Desklet * item;
  g_assert(desc->id);
  for (i=0; i<conf->desklets->len; ++i) {
    item = &g_array_index(conf->desklets, Desklet, i);
    if (g_ascii_strcasecmp(desc->id, item->id)==0)
      break;
  }
  if (i == conf->desklets->len)
    g_array_append_val(conf->desklets, (*desc));
  return i;
}

/*---------------------------------------------------------------------------*/
void 
config_dump(EConfig ** conf) {
  GTimeVal tv;
  gchar * out, *group, **groups, *isotime, *comment;
  Desklet * item;
  gint i, j;
  gsize n;
  LFILE * fout;

  if (!conf || !(*conf)) return;
  if ((*conf)->crashmode)
    goto end;

#define kf ((*conf)->kf)

  /* ...top comment */
  g_get_current_time(&tv);
  isotime = g_time_val_to_iso8601(&tv);
  comment = g_strdup_printf(_("\
 Edit at your own risk! (last modified: %s)\n"), isotime);
  g_key_file_set_comment(kf, NULL, NULL, comment, NULL);
  g_free(comment);
  g_free(isotime);

  /* ...existing values */
  for(i=0; i<(*conf)->desklets->len;++i) {
    group = g_strdup_printf("desklet_%d", i);
    item = &g_array_index((*conf)->desklets, Desklet, i);
    if (item->id)
      g_key_file_set_string (kf, group, "id"      , item->id);
    if (item->uri)
      g_key_file_set_string (kf, group, "uri"     , item->uri);
    if (item->x >=0)
      g_key_file_set_integer(kf, group, "x"       , item->x);
    if (item->y >=0)
      g_key_file_set_integer(kf, group, "y"       , item->y);
    g_key_file_set_integer(kf, group, "width"   , item->width);
    g_key_file_set_integer(kf, group, "height"  , item->height);
    g_key_file_set_boolean(kf, group, "decorate", item->decorate);
    g_key_file_set_integer(kf, group, "timeout" , item->timeout);
    g_key_file_set_integer(kf, group, "attempts", item->attempts);   
    g_free(group);
  }

  /* ...strip removed values */
  groups = g_key_file_get_groups(kf, &n);
  for(i=0; i<n; ++i)
    if ((g_ascii_strcasecmp("general", groups[i])!=0) && 
	((g_strstr_len(groups[i], 8, "desklet_")==NULL) || 
	 (atoi(groups[i]+8) >= (*conf)->desklets->len)))
      g_key_file_remove_group(kf, groups[i], NULL);

  /* Dump it to file */
  out = g_key_file_to_data(kf, &n, NULL);
  fseek(file((*conf)->lf), 0, SEEK_SET);
  fwrite(out, n, 1, file((*conf)->lf));
  ftruncate(fileno(file((*conf)->lf)), n); 
  g_free(out);
  
 end:
  fclose_locked(&(*conf)->lf, NULL);
  if (kf) g_key_file_free(kf);
  g_array_free((*conf)->desklets, TRUE);
  if ((*conf)->crashmsg) g_free((*conf)->crashmsg);
  g_free(*conf);
  *conf = NULL;

#undef kf
}

/*---------------------------------------------------------------------------*/
