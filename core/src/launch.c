/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

#ifdef HAVE_SYS_TYPES_H
#include <sys/types.h>
#endif

#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif

#ifdef HAVE_SIGNAL_H
#include <signal.h>
#endif

void
launch_register(gboolean nolock) {
  gchar * fn;
  pid_t id;
  LFILE * lf;
  GError * err = NULL;

  fn = config_path("pids");
  if ((lf = fopen_locked(fn, "a", !nolock, TRUE, &err))) {
    fseek(file(lf), 0, SEEK_END);
    g_fprintf(file(lf), "%d\n", getpid());
    fclose_locked(&lf, NULL);
  } else
    g_error(err->message);
}

void
launch_killall(gboolean nolock) {
  gchar * fn;
  LFILE * lf;
  gint pid;
  GError * err = NULL;
  
  fn = config_path("pids");
  if ((lf = fopen_locked(fn, "r+", !nolock, TRUE, &err))) {
    while (fscanf(file(lf), "%d", &pid) == 1)
      kill(pid, SIGQUIT);
    ftruncate(fileno(file(lf)), 0);
    fclose_locked(&lf, NULL);
  } else {
    g_warning(err->message);
    g_error_free(err);
  }
}

void
launch_killall_external(gchar * fullname, gboolean nolock) {
  GPtrArray * args;
  args = g_ptr_array_new();
  g_ptr_array_add(args, (gpointer)g_strdup(fullname));
  g_ptr_array_add(args, (gpointer)g_strdup("--killall"));
  if (nolock)
    g_ptr_array_add(args, (gpointer)g_strdup("--nolock"));
  g_ptr_array_add(args, NULL);
  g_spawn_async(NULL,
		(gchar**)args->pdata,
		NULL, 0, NULL, NULL, NULL, NULL);
  g_ptr_array_free(args, TRUE);
}

void
launch_all(gchar * fullname, gboolean nolock) {
  EConfig * conf;
  Desklet * item;
  GPtrArray * args;
  gint i;

  if ((conf = config_load(nolock, TRUE))) {
    if (conf->crashmsg)
      g_error(conf->crashmsg);
    launch_killall(nolock);

    /* Make sure the children won't act as launcher 
       even if  fullname is 'edesklets-start' */
    g_setenv("EDESKLETS_NOSTART", "1", TRUE);

    /* Time to launch desklets one by one... */
    for(i=0; i<conf->desklets->len; ++i) {     
      /* ...Build the command line */
      item = &g_array_index(conf->desklets, Desklet, i);      
      args = g_ptr_array_new();

      g_ptr_array_add(args,   (gpointer)g_strdup(fullname));
      if (nolock)
	g_ptr_array_add(args, (gpointer)g_strdup("--nolock"));
      if (item->uri)
	g_ptr_array_add(args, (gpointer)g_strdup_printf("--uri=%s", 
							item->uri));

      g_ptr_array_add(args,   (gpointer)g_strdup_printf("--pos_x=%d", 
							item->x));
      g_ptr_array_add(args,   (gpointer)g_strdup_printf("--pos_y=%d", 
							item->y));
      g_ptr_array_add(args,   (gpointer)g_strdup_printf("--width=%d", 
							item->width));
      g_ptr_array_add(args,   (gpointer)g_strdup_printf("--height=%d",
							item->height));
      if (item->decorate)
	g_ptr_array_add(args, (gpointer)g_strdup_printf("--decorate"));
      g_ptr_array_add(args,   (gpointer)g_strdup_printf("--timeout=%d",
							item->timeout));
      g_ptr_array_add(args,   (gpointer)g_strdup_printf("--attempts=%d",
							item->attempts));
      g_ptr_array_add(args, NULL);

      /* Set ID in environment */
      g_setenv("EDESKLETS_ID", item->id, TRUE);

      /* ...Spawn it */
      g_spawn_async(NULL,
		   (gchar**)args->pdata,
		   NULL, 0, NULL, NULL, NULL, NULL);
      g_ptr_array_free(args, TRUE);

      /* Wait in case of missing lock */
      if (nolock) sleep(1);
    }
    config_dump(&conf);
  }
}

void
launch_all_external(gchar * fullname, gboolean nolock) {
  GPtrArray * args;
  args = g_ptr_array_new();
  g_ptr_array_add(args, (gpointer)g_strdup(fullname));
  g_ptr_array_add(args, (gpointer)g_strdup("--start"));
  if (nolock)
    g_ptr_array_add(args, (gpointer)g_strdup("--nolock"));
  g_ptr_array_add(args, NULL);
  g_spawn_async(NULL,
		(gchar**)args->pdata,
		NULL, 0, NULL, NULL, NULL, NULL);
  g_ptr_array_free(args, TRUE);
}
