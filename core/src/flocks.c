/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

/*
  Abstraction for fcntl / flock file locking
 */
#include "edesklets.h"

/*---------------------------------------------------------------------------*/
#ifdef HAVE_FCNTL
#ifdef HAVE_UNISTD_H
#include <unistd.h>
#endif
#ifdef HAVE_FCNTL_H
#include <fcntl.h>
#endif
#else
#ifdef HAVE_FLOCK
#ifdef HAVE_SYS_FILE_H
#include <sys/file.h>
#endif
#endif
#endif

/*---------------------------------------------------------------------------*/
struct _LFILE {
  FILE * f;
  gboolean locked;
};

/*---------------------------------------------------------------------------*/
FILE * file(LFILE * lf) {return (lf)?lf->f:NULL;}

LFILE * 
fopen_locked(gchar * filename, gchar * mode, 
	     gboolean exclusive, gboolean block, 
	     GError ** error) {
  LFILE * lf;
  char * errmsg = NULL;
  EError errnum;
  gboolean errmsg_free;
#ifdef HAVE_FCNTL
  int flags;
  struct flock lock;
#endif

#define fatal(ERRNUM) \
  do{\
    errnum = ERRNUM;\
    errmsg = error_system();\
    errmsg_free = FALSE;\
    goto eexit;\
  }while(0)

#define bailout(ERRNUM, ...)			\
  do{\
    errnum = ERRNUM;\
    errmsg = g_strdup_printf(__VA_ARGS__);\
    errmsg_free = TRUE;\
    goto eexit;\
  }while(0)
  
  if (!(lf = g_malloc0(sizeof(LFILE))))
    bailout(EERROR_UNDEFINED, _("could not allocate memory"));

  if (!(lf->f = fopen(filename, mode)))
    fatal(EERROR_SYSTEM);

  if (exclusive) {
#ifdef HAVE_FCNTL
    lock.l_type = F_WRLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 0;
    if (fcntl(fileno(lf->f),(block)?F_SETLKW:F_SETLK, &lock)!=0) {
      if (!block && (errno == EACCES || errno == EAGAIN))
	fatal(EERROR_ALREADY_LOCKED);
      else
	fatal(EERROR_SYSTEM);
    }
    lf->locked = TRUE;
#else
#ifdef HAVE_FLOCK
    if (flock(fileno(lf->f), LOCK_EX | ((block)?0:LOCK_NB)) == -1)
      if (!block && errno == EWOULDBLOCK)
	fatal(EERROR_ALREADY_LOCKED);
      else
	fatal(EERROR_SYSTEM);
    lf->locked = TRUE;
#else
    bailout(EERROR_ALREADY_LOCKED, _("No lock mechanism available"));
#endif
#endif
  }
  return lf;

 eexit:
  if (lf) {
    if (lf->f) fclose(lf->f);
    g_free(lf);
  }
  if (error)
    *error = g_error_new(*EDOMAIN, errnum, "%s", errmsg);
  if (errmsg_free) 
    g_free(errmsg);
  return NULL;

#undef bailout
#undef fatal
}

void
fclose_locked(LFILE ** llf, GError ** error) {
#ifdef HAVE_FCNTL
  struct flock lock;
#endif
  if (!llf || !(*llf)) return;
  g_assert((*llf)->f);

#define fatal() \
do{\
  if (error && *error == NULL)\
    *error = g_error_new(*EDOMAIN, EERROR_SYSTEM, "%s", error_system());\
 } while(0)

  if ((*llf)->locked) {
#ifdef HAVE_FCNTL
    lock.l_type = F_UNLCK;
    lock.l_whence = SEEK_SET;
    lock.l_start = 0;
    lock.l_len = 0;
    if (fcntl(fileno((*llf)->f),F_SETLK,&lock) == -1)
      fatal();
#else
#ifdef HAVE_FLOCK
    if (flock(fileno((*llf)->f), LOCK_UN) == -1)
	fatal();
#endif
#endif
  }
  if (fclose((*llf)->f) != 0)
    fatal();
  
  g_free(*llf);
  *llf = NULL;

#undef fatal
}
