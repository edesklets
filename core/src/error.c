/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

#ifdef HAVE_STRERROR
#ifdef HAVE_STRING_H
#include <string.h>	/* sterror() */
#endif
#endif

GQuark _edomain = 0;
const GQuark * EDOMAIN = &_edomain;

char * error_system(void) {
#ifdef HAVE_STRERROR
  return strerror(errno);
#else
  static char default_err[] = "undefined error";
#ifdef HAVE_SYS_ERRLIST
  return (char*)((errno < sys_nerr)?sys_errlist[errno]:default_err);
#else
  return default_err;
#endif
#endif
}

int error_num(void) {
  return errno;
}

void 
error_init(void) {
  _edomain = g_quark_from_string(PACKAGE);
}

