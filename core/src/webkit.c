/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"
#include <webkit/webkit.h>

/*---------------------------------------------------------------------------*/
typedef struct _WebkitStates {
  GtkWidget * window,
    * web_view, * status;
  gchar * fullname;
  Desklet * desc;
  gboolean nolock;
  gchar * uri, * text;
  gint timeout, tag, tic, try;
} WebkitStates;

/*---------------------------------------------------------------------------*/
static const gchar * WEBKIT_TEMPLATE = "<html>\n\
<style type=\"text/css\">\n\
p {\n\
background-color: rgba(200, 200, 200, .5);\n\
color: black;\n\
padding: 5px;\n\
text-align: center;\n\
font-size: x-large;\n\
border: 1px dashed black;\n\
};\n\
</style>\n\
<body>\n\
<p>%s</p>\n\
</body>\n\
</html>";

/*---------------------------------------------------------------------------*/
static void webkit_request(WebkitStates * states);

static void
webkit_about(WebkitStates * states) {
  about_dialog(states->window);
}

static void
webkit_preferences(WebkitStates * states) {
  prefs_dialog(states->window, states->fullname, 
	       states->desc, states->nolock);
}

static void
webkit_reload(WebkitStates * states) {
  launch_all_external(states->fullname, states->nolock);
}

static void
webkit_quit_all(WebkitStates * states) {
  if (dialog_yesno(states->window, _("Really quit all desklets?")))
    launch_killall_external(states->fullname, states->nolock);
}

static void
webkit_quit(WebkitStates * states) {
  /* See webkit_free for cleanup */
  if (dialog_yesno(states->window, _("Really exit?")))
    gtk_widget_destroy(states->window);
}

static void
webkit_popup(GtkWidget * web_view, GdkEventButton * event, 
	     WebkitStates * states) {
  GtkWidget * menu, *item, * img;

  menu = gtk_menu_new();

#define MENU_ENTRY(mnemonic, stock, callback) \
  do{\
    item = gtk_image_menu_item_new_with_mnemonic(mnemonic);\
    gtk_image_menu_item_set_image(GTK_IMAGE_MENU_ITEM(item),\
       gtk_image_new_from_stock(GTK_STOCK_ ## stock,\
				GTK_ICON_SIZE_BUTTON));\
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);\
    if (callback)\
      g_signal_connect_swapped(G_OBJECT(item), "activate",\
			       G_CALLBACK(callback), states);\
  } while(0)

#define MENU_SEP()\
  do {\
    item = gtk_separator_menu_item_new();\
    gtk_menu_shell_append(GTK_MENU_SHELL(menu), item);\
  } while(0)

  MENU_ENTRY(_("_About eDesklets"), ABOUT, webkit_about);
  MENU_SEP();
  MENU_ENTRY(_("_Configure"), PREFERENCES, webkit_preferences);
  MENU_ENTRY(_("_Reload All"), REFRESH, webkit_reload);
  MENU_SEP();
  MENU_ENTRY(_("_Kill All Desklets"), CANCEL, webkit_quit_all);
  MENU_ENTRY(_("_Exit Desklet"), QUIT, webkit_quit);

#undef MENU_SEP
#undef MENU_ENTRY

  gtk_widget_show_all(menu);
  gtk_menu_popup(GTK_MENU(menu), NULL, NULL, NULL, NULL, 3, 0);
}

static gboolean
webkit_free(GtkWidget * window, WebkitStates * states) {
  g_assert(states);
  g_free(states);
  gtk_main_quit();
  return TRUE;
}

static gboolean
webkit_button_press(GtkWidget * web_view, GdkEventButton * event, 
		    WebkitStates * states) {
  if (event->button==3) {
    webkit_popup(web_view, event, states);
    return TRUE;
  }
  return FALSE;
}

static gboolean
webkit_button_release(GtkWidget * web_view, GdkEventButton * event, 
		      WebkitStates * states) {  
  /* Workaround found thanks to valgrind: it seems WebKit (r30763) needs to be
     explicitely told to stop event propagation on right mouse button to avoid a
     segfault if first click is RMB... Weird. */
  return (event->button==3);
}

static void 
webkit_message(GtkWidget * web_view, gchar * msg) {
  gchar * tmp = g_strdup_printf(WEBKIT_TEMPLATE, msg);
  webkit_web_view_load_html_string(WEBKIT_WEB_VIEW(web_view), tmp, "about:");
  g_free(tmp);
}

static void
webkit_commit(WebKitWebView* web_view, WebKitWebFrame* frame, 
	      WebkitStates * states) {
  if (states->tag > 0) g_source_remove(states->tag);
  gtk_widget_hide(states->status);
}

static gint
webkit_tic(WebkitStates * states) {
  if (--states->tic > 0) {
    g_free(states->text);
    states->text = g_strdup_printf("[%d] %s", states->tic, states->uri);
    gtk_label_set_text(GTK_LABEL(states->status), states->text);
  } else {
    if (--states->try != 0) {
      g_source_remove(states->tag);
      webkit_web_view_stop_loading(WEBKIT_WEB_VIEW(states->web_view));
      webkit_request(states);
    } else {
      webkit_web_view_stop_loading(WEBKIT_WEB_VIEW(states->web_view));
      webkit_message(states->web_view, _("Content not available (timeout)"));
    }
  }
  return (states->tic > 0);
}

static void
webkit_request(WebkitStates * states) {
  if (states->timeout > 0) {
    gtk_widget_show(states->status);
    gtk_label_set_text(GTK_LABEL(states->status),
		       states->text = g_strdup_printf("%s", states->uri));
    states->tic = states->timeout;
    states->tag = g_timeout_add(1000, (GSourceFunc)webkit_tic, states);
  } else
    states->tag = 0;
  webkit_web_view_open(WEBKIT_WEB_VIEW(states->web_view), states->uri);
}

/*---------------------------------------------------------------------------*/
void
webkit_launch(gchar * fullname, Desklet * desc, gboolean nolock) {
  WebkitStates * states;
  GtkWidget    * box, * ebox;
  GdkScreen    * screen;
  GdkColormap  * colormap;

  if (!(states = g_malloc0(sizeof(WebkitStates))))
    g_error("could not allocate memory");

  states->fullname = fullname;
  states->desc = desc; 
  states->nolock = nolock;
  states->web_view = webkit_web_view_new();
  states->status = gtk_label_new(NULL);
  webkit_web_view_set_transparent(WEBKIT_WEB_VIEW(states->web_view), TRUE);

  states->window = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  box    = gtk_vbox_new(FALSE, 0);
  
  g_signal_connect(G_OBJECT(states->window), "destroy", 
		   G_CALLBACK(webkit_free), states);
  gtk_window_set_decorated(GTK_WINDOW(states->window), desc->decorate);
  gtk_window_set_default_size(GTK_WINDOW(states->window), 
			      desc->width, desc->height);
  if (desc->x >= 0 && desc->y >= 0)
    gtk_window_move(GTK_WINDOW(states->window), desc->x, desc->y);

  gtk_container_add(GTK_CONTAINER(states->window), box);
  ebox = gtk_event_box_new();
  gtk_container_add(GTK_CONTAINER(ebox), states->status);
  gtk_box_pack_start(GTK_BOX(box), ebox, FALSE, FALSE, 0);
  gtk_box_pack_end(GTK_BOX(box), states->web_view, TRUE, TRUE, 0);
    
  screen = gdk_screen_get_default();
  colormap = gdk_screen_get_rgba_colormap(screen);
  if (!colormap) {
    g_warning(_("No RGBA colormap found; try enabling compositing"));
    colormap = gdk_screen_get_rgb_colormap(screen);
  }
  gtk_widget_set_colormap(states->window, colormap);

  gtk_widget_show_all(states->window);

  g_signal_connect(states->web_view, "popup-menu", 
		   G_CALLBACK(webkit_popup), states);
  g_signal_connect(ebox, "button-press-event", 
		   G_CALLBACK(webkit_button_press), states);
  g_signal_connect(states->web_view, "button-press-event", 
		   G_CALLBACK(webkit_button_press), states);
  g_signal_connect(states->web_view, "button-release-event", 
		   G_CALLBACK(webkit_button_release), states);  
  g_signal_connect(states->web_view, "load-committed", 
		   G_CALLBACK (webkit_commit), states);

  if (desc->uri) {
    states->uri = desc->uri;
    states->timeout = desc->timeout;
    states->try = desc->attempts;
    webkit_request(states);
  } else
    webkit_message(states->web_view, 
		   _("No URI specified (right click to configure)"));
}
