/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

static gint
dialog_message(GtkWidget * parent,
	       GtkMessageType type,
	       GtkButtonsType buttons,
	       gchar * title,
	       gchar * msg) {
  gint ret;
  GtkWidget * dlg;
  dlg = gtk_message_dialog_new(GTK_WINDOW(parent),
			       GTK_DIALOG_DESTROY_WITH_PARENT,
			       type,
			       buttons,
			       msg);
  gtk_window_set_title(GTK_WINDOW(dlg), title);
  ret = gtk_dialog_run(GTK_DIALOG(dlg));
  gtk_widget_destroy(dlg); 
  return ret;
}

void
dialog_error(GtkWidget * parent, gchar * msg) {
  dialog_message(parent, GTK_MESSAGE_ERROR, GTK_BUTTONS_CLOSE, "Error", msg);
}

void
dialog_warning(GtkWidget * parent, gchar * msg) {
  dialog_message(parent, GTK_MESSAGE_WARNING, GTK_BUTTONS_CLOSE, "Warning", msg);
}

gboolean
dialog_yesno(GtkWidget * parent, gchar * msg) {
  return (dialog_message(parent, 
			 GTK_MESSAGE_QUESTION, GTK_BUTTONS_YES_NO, 
			 "Question", msg) == GTK_RESPONSE_YES);
}


