/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

/*---------------------------------------------------------------------------*/
#include "edesklets.h"

/*---------------------------------------------------------------------------*/
static GOptionContext *
populate_command_line(Desklet * desc,
		      gboolean has_display,
		      gboolean * version,
		      gboolean * start,
		      gboolean * killkill,
		      gboolean * nolock,
		      gboolean * justregister,
		      gboolean * wait) {
  GOptionEntry single[] = {
    {"uri"     , 'u', 0, G_OPTION_ARG_STRING, &desc->uri, 
     _("URI to load from"), NULL},
    {"pos_x"   , 'x', 0, G_OPTION_ARG_INT   , &desc->x, 
     _("X position of desklet"), NULL},
    {"pos_y"   , 'y', 0, G_OPTION_ARG_INT   , &desc->y, 
     _("Y position of desklet"), NULL},
    {"width"   , 'w', 0, G_OPTION_ARG_INT   , &desc->width, 
     _("Width of desklet"), NULL},
    {"height"  , 'h', 0, G_OPTION_ARG_INT   , &desc->height, 
     _("Height of desklet"), NULL},
    {"decorate", 'd', 0, G_OPTION_ARG_NONE  , &desc->decorate, 
     _("Decorate the window"), NULL},
    {"timeout" , 't', 0, G_OPTION_ARG_INT   , &desc->timeout,
     _("URI load timeout (in seconds)")},
    {"attempts", 'a', 0, G_OPTION_ARG_INT   , &desc->attempts,
     _("Number of time loading from URI should be attempted")},
    {NULL}
  };
  GOptionEntry general[] = {
    {"version", 'v', 0, G_OPTION_ARG_NONE, version,
     _("Print version info and exit")},
    {"start", 's', 0, G_OPTION_ARG_NONE, start, 
     _("(Re)start all desklets (same as edesklets-start)")},
    {"killall", 'k', 0, G_OPTION_ARG_NONE, killkill,
     _("Kill all the running desklets and exit")},
    {"nolock", 0, 0, G_OPTION_ARG_NONE, nolock,
     _("Do not lock config files when reading or writing")},
    {"justregister", 'j', 0, G_OPTION_ARG_NONE, justregister,
     _("Register a new desklet using the specified parameters and exit")},
    {"wait", 0, 0, G_OPTION_ARG_INT, wait,
     _("wait specified delay before executing the command")},
    {NULL}
  };
  GOptionGroup * sgroup, * ggroup;
  GOptionContext * ctx;

  ctx = g_option_context_new(NULL);

  if (!*start) {
    sgroup = g_option_group_new("desklet", _("Options for a Single Desklet"), 
				_("Show Individual Desklet Options"), NULL, NULL);
    g_option_group_add_entries(sgroup, single);
    g_option_context_set_main_group(ctx, sgroup);
  }
  ggroup = g_option_group_new("misc", _("Miscellaneous eDesklets Options"),
			      _("Show Other eDesklets Options"), NULL, NULL);
  g_option_group_add_entries(ggroup, general);
  if (!*start)
    g_option_context_add_group(ctx, ggroup);
  else
    g_option_context_set_main_group(ctx, ggroup);

  g_option_context_add_group(ctx, gtk_get_option_group(has_display));

  return ctx;
}

/*---------------------------------------------------------------------------*/
int
main(int argc, char **argv) {
  gboolean nolock, has_display,
    start = FALSE, justregister = FALSE, version = FALSE, killkill = FALSE;
  gchar * id;
  gint wait = 0;
  gchar * basename, * fullname;
  Desklet desc;
  GError *err = NULL;
  EConfig * conf;
  GOptionContext * ctx;

  /* Set error domain */
  error_init();
  gtk_set_locale();
  bindtextdomain(PACKAGE, LOCALEDIR);
  textdomain(PACKAGE);

  /* Read a few default values from the environment */
  nolock = (gboolean)(g_getenv("EDESKLETS_NOLOCK")!=NULL);
  start = (g_ascii_strcasecmp(basename = 
			      g_path_get_basename(
			         fullname = g_strdup(argv[0])),
			      "edesklets-start") == 0) &&
    !g_getenv("EDESKLETS_NOSTART");
  g_free(basename);
  id = ((basename = (gchar*)g_getenv("EDESKLETS_ID")))?
    g_strdup(basename):id_generate();

  /* Build the command line */
  config_default(&desc);
  desc.id = id;
  has_display = gtk_init_check(&argc, &argv);
  ctx = populate_command_line(&desc, 
			      has_display,
			      &version, 
			      &start,
			      &killkill,
			      &nolock, 
			      &justregister, 
			      &wait);

  /* Parse the command line */
  if (g_option_context_parse(ctx, &argc, &argv, &err) == FALSE)
    g_error("%s\n", err->message);
  g_option_context_free(ctx);

  /* Printout version info, if applicable */
  if (version) {
    g_printf(about_text());
    return 0;
  }

  /* Wait for the specified delay, if applicable */
  if (wait > 0) sleep(wait);

  /* Time to perform the wanted action... */
  if (killkill) {
    /* ...kill everything. */
    launch_killall(nolock);
  } else if (start) {
    /* ...(re)launch everything. */
    launch_all(fullname, nolock);
  } else if (justregister) {
    /* ...register desklet, and exit. */
    conf = config_load(nolock, FALSE);
    if (conf->crashmode)
      g_error("%s", conf->crashmsg);
    config_merge(conf, &desc);
    config_dump(&conf);
  } else {
    /* ...start a desklet. */
    if (!has_display)
      g_error(_("cannot open display"));
    launch_register(nolock);
    icons_init();
    webkit_launch(fullname, &desc, nolock);
    gtk_main();
  }
  g_free(fullname);

  return 0;
}

/*---------------------------------------------------------------------------*/
