/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

void
icons_init(void) {
#ifndef _WIN32
  gint sizes[] = {16, 32, 48, 64, 128, 0};
  gchar * path, * base;
  gint i;
  GList * icons = NULL;
  GdkPixbuf * icon;

  for(i=0; sizes[i]; ++i) {
    base = g_strdup_printf("%dx%d", sizes[i], sizes[i]);
    path = g_build_filename(DATADIR, "icons", "hicolor", 
			    base, "apps", "edesklets.png", NULL);
    if ((icon = gdk_pixbuf_new_from_file(path, NULL)))
      icons = g_list_append(icons, icon);
    g_free(path);
  }

  if (icons) {
    gtk_window_set_default_icon_list(icons);
    g_list_foreach(icons, (GFunc)g_object_unref, NULL);
    g_list_free(icons);
  } else
    g_warning("could not load window icons");

#endif
}
