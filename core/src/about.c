/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

static void
about_dialog_quit(GtkWidget * button, GtkWidget * dlg) {
  gtk_widget_destroy(dlg);
}

gchar *
about_text(void) {
  return g_strdup_printf(
     "eDesklets %s\n\n%s\n%s\n",
     VERSION,
     _("Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>"),
     _("This is free software.  You may redistribute copies of it under\n" 
       "the terms of the GNU General Public License\n"
       "<http://www.gnu.org/licenses/gpl.html>.\n\n"
       "There is NO WARRANTY, to the extent permitted by law.")
			 );
}

void
about_dialog(GtkWidget * parent) {
  GtkWidget * dlg, * hbox, * vbox, * item;

  dlg = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_modal(GTK_WINDOW(dlg), TRUE);
  gtk_window_set_resizable(GTK_WINDOW(dlg), FALSE);
  gtk_window_set_title(GTK_WINDOW(dlg), _("About eDesklets"));

  vbox = gtk_vbox_new(FALSE, 0);
  gtk_container_add(GTK_CONTAINER(dlg), vbox);
  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

  item = gtk_image_new_from_stock(GTK_STOCK_ABOUT, GTK_ICON_SIZE_DIALOG);
  gtk_box_pack_start(GTK_BOX(hbox), item, FALSE, FALSE, 5);

  item = gtk_label_new(about_text());
  gtk_box_pack_start(GTK_BOX(hbox), item, FALSE, FALSE, 5);

  hbox = gtk_hbox_new(FALSE, 0);
  gtk_box_pack_end(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

  item = gtk_button_new_from_stock(GTK_STOCK_OK);
  g_signal_connect(G_OBJECT(item), "clicked", 
		   G_CALLBACK(about_dialog_quit), dlg);
  gtk_box_pack_end(GTK_BOX(hbox), item, FALSE, FALSE, 5);

  gtk_widget_show_all(dlg);
}
