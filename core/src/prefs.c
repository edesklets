/*-----------------------------------------------------------------------------
Copyright (C) Sylvain Fourmanoit <syfou@users.sourceforge.net>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to
deal in the Software without restriction, including without limitation the
rights to use, copy, modify, merge, publish, distribute, sublicense, and/or
sell copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies of the Software and its documentation and acknowledgment shall be
given in the documentation and software packages that this Software was
used.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.   
------------------------------------------------------------------------------*/

#include "edesklets.h"

#ifdef HAVE_STRING_H
#include <string.h>
#endif

typedef struct _EPrefs {
  EConfig * conf;
  gchar * fullname;
  gboolean nolock;
  GtkWidget * dlg, * da, * lv,
    * uri, 
    * x, * y, * width, * height, 
    * decorate, 
    * timeout, * attempts;
  gint current, self, last;
  gboolean savemode;
  gint w, h;
} EPrefs;

static void
prefs_preview_draw(EPrefs * prefs, GdkGC * gc, 
		   gint i, gint mw, gint mh) {
  Desklet * item;
  item = &g_array_index(prefs->conf->desklets, Desklet, i);    
  gdk_draw_rectangle(prefs->da->window,
		     gc,
		     TRUE,
		     item->x * mw / prefs->w,
		     item->y * mh / prefs->h,
		     item->width * mw / prefs->w + 1,
		     item->height * mh / prefs->h + 1);
}

static void
prefs_preview(EPrefs * prefs) {
  const GdkColor white = {0, 65535, 65535, 65535};
  const GdkColor blue  = {0, 0, 0, 65535};
  const GdkColor black = {0, 0, 0, 0};
  gint i,
    mw, mh;
  GdkGC * gc;

  mw = prefs->da->allocation.width; mh = prefs->da->allocation.height;
  
  gc = gdk_gc_new(GDK_DRAWABLE(prefs->da->window));
  gdk_gc_set_rgb_fg_color(gc, &white);
  gdk_draw_rectangle(prefs->da->window, gc, TRUE, 0, 0, mw, mh);

  gdk_gc_set_rgb_fg_color(gc, &black);

  for (i=0; i<prefs->conf->desklets->len; ++i)
    if (i != prefs->current)
      prefs_preview_draw(prefs, gc, i, mw, mh);

  gdk_gc_set_rgb_fg_color(gc, &blue);
  prefs_preview_draw(prefs, gc, prefs->current, mw, mh);

  gdk_gc_destroy(gc);
}

static gboolean
prefs_preview_refresh(GtkWidget * da, GdkEventExpose * event, EPrefs * prefs) {
  prefs_preview(prefs);
  return TRUE;
}

static void
prefs_get_geometry(gint * width, gint * height) {
  GdkScreen * scr;
  g_assert(width); g_assert(height);
  scr     = gdk_screen_get_default();
  *width  = gdk_screen_get_width(scr);
  *height = gdk_screen_get_height(scr);
}

static void
prefs_enable_form(EPrefs * prefs, gboolean enable) {
#define SET_FORM(field) \
  do {\
  gtk_widget_set_sensitive(prefs->field, enable);\
  } while(0)

  SET_FORM(uri);
  SET_FORM(x);
  SET_FORM(y);
  SET_FORM(width);
  SET_FORM(height);
  SET_FORM(decorate);
  SET_FORM(timeout);
  SET_FORM(attempts);

#undef SET_FORM
}

static void
prefs_save_form(GtkWidget * widget, EPrefs * prefs) {
  Desklet * item;
  gchar * uri;
  gint val;
  
  if (prefs->savemode) {
    item = &g_array_index(prefs->conf->desklets, Desklet, prefs->current);
    if (widget == prefs->uri) {
      uri = g_strdup(gtk_entry_get_text(GTK_ENTRY(prefs->uri)));
      if (item->uri) g_free(item->uri);
      if (strlen(g_strstrip(uri)) > 0)
	item->uri = uri;
      else {
	item->uri = NULL;
	g_free(uri);
      }
      goto exit;
    }
    if (widget == prefs->decorate) {
      item->decorate = gtk_toggle_button_get_active(
        GTK_TOGGLE_BUTTON(prefs->decorate));
      goto exit;
    }

    val = gtk_spin_button_get_value_as_int(GTK_SPIN_BUTTON(widget));

#define SAVE_VAL(field, label) \
    if (widget == prefs->field) {\
      item->field = val;\
      goto label;\
    }
    SAVE_VAL(x       , update);
    SAVE_VAL(y       , update);
    SAVE_VAL(width   , update);
    SAVE_VAL(height  , update);
    SAVE_VAL(attempts, exit);
    SAVE_VAL(timeout , exit);
#undef SAVE_VAL

  update:
    prefs_preview(prefs);
  }

 exit:
  return;
}

static void
prefs_update_form(EPrefs * prefs) {
  Desklet * item;

  prefs->savemode = FALSE;

  item = &g_array_index(prefs->conf->desklets, Desklet, prefs->current);

  gtk_entry_set_text(GTK_ENTRY(prefs->uri), (item->uri)?item->uri:"");

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(prefs->x), item->x);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(prefs->y), item->y);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(prefs->width), item->width);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(prefs->height), item->height);
  
  gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(prefs->decorate), 
			       item->decorate);

  gtk_spin_button_set_value(GTK_SPIN_BUTTON(prefs->timeout), item->timeout);
  gtk_spin_button_set_value(GTK_SPIN_BUTTON(prefs->attempts), item->attempts);
  
  prefs->savemode = TRUE;
}

static void
prefs_viewlist_select(GtkTreeSelection * sel, EPrefs * prefs) {
  GtkTreeIter iter;
  GtkTreePath * path;
  GtkTreeModel * model;
  gint * idx;

  if (gtk_tree_selection_get_selected(sel, &model, &iter)) {
    path = gtk_tree_model_get_path(model, &iter);
    idx = gtk_tree_path_get_indices(path);
    if (idx) {
      prefs->current = idx[0];
      prefs_update_form(prefs);
      prefs_enable_form(prefs, TRUE);
      prefs_preview(prefs);
    }
    gtk_tree_path_free(path);
  }
}

static void
prefs_create_listview(EPrefs * prefs) {
  GtkCellRenderer * render;
  GtkTreeViewColumn * col;
  GtkTreeSelection * sel;
  GtkListStore * ls;
  GtkTreeModel *model;
  GtkTreeIter row;
  GtkWidget * view;
  gint i;

  ls = gtk_list_store_new(1, G_TYPE_STRING);

  prefs->lv = view = gtk_tree_view_new_with_model(GTK_TREE_MODEL(ls));

  render = gtk_cell_renderer_text_new();
  col = gtk_tree_view_column_new();

  gtk_tree_view_column_pack_start(col, render, TRUE);
  gtk_tree_view_column_add_attribute(col, render, "text", 0);
  gtk_tree_view_column_set_title(col, _("Desklets"));

  gtk_tree_view_append_column(GTK_TREE_VIEW(view), col);

  sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(view));
  gtk_tree_selection_set_mode(sel, GTK_SELECTION_SINGLE);

  /* Populate */
  model = gtk_tree_view_get_model(GTK_TREE_VIEW(view));
  for(i=0; i<prefs->conf->desklets->len; ++i) {
    gtk_list_store_append(GTK_LIST_STORE(model), &row);
    gtk_list_store_set(GTK_LIST_STORE(model), &row, 0, 
		       g_strdup_printf((prefs->self==i)?_("%d (self)"):"%d", i), 
		       -1);
  }
  g_signal_connect(sel, "changed", G_CALLBACK(prefs_viewlist_select), prefs);
}

static gboolean
prefs_dialog_free(GtkWidget * dlg, EPrefs * prefs) {
  if (prefs->conf) {
    prefs->conf->crashmode = TRUE; /* force no save */
    config_dump(&prefs->conf);
  }
  g_free(prefs);
  return TRUE;
}

static void
prefs_dialog_cancel(GtkWidget * button, EPrefs * prefs) {
  if (dialog_yesno(prefs->dlg, 
		   _("Exit configuration without saving "
		     "(all changes will be lost)?")))
    gtk_widget_destroy(prefs->dlg);
}

static void
prefs_dialog_save(GtkWidget * button, EPrefs * prefs) {

  config_dump(&prefs->conf);
  g_printf("Saved\n");

  if (dialog_yesno(prefs->dlg, _("Reload all desklets now?")))
    launch_all_external(prefs->fullname, prefs->nolock);

  gtk_widget_destroy(prefs->dlg);
}

static void
prefs_dialog_add(GtkWidget * button, EPrefs * prefs) {
  Desklet item;
  GtkTreeModel *model;
  GtkTreeIter   row;

  config_default(&item);
  item.id = id_generate();

  g_array_append_val(prefs->conf->desklets, item);

  model = gtk_tree_view_get_model(GTK_TREE_VIEW(prefs->lv));
  gtk_list_store_append(GTK_LIST_STORE(model), &row);

  gtk_list_store_set(GTK_LIST_STORE(model), &row, 0, 
		     g_strdup_printf("%d", 1 + prefs->last++), -1);

  prefs_preview(prefs);  
}

static void
prefs_dialog_del(GtkWidget * button, EPrefs * prefs) { 
  GtkTreeIter iter;
  GtkTreeModel * model;
  GtkTreeSelection * sel;
  
  GtkTreeIter row;

  sel = gtk_tree_view_get_selection(GTK_TREE_VIEW(prefs->lv));

  if (sel && 
      (prefs->current >= 0) && 
      gtk_tree_selection_get_selected(sel, &model, &iter)) {

    if (dialog_yesno(prefs->dlg, 
		     _("Are you sure you want to delete selected desklet?"))) {
      gtk_list_store_remove(GTK_LIST_STORE(model), &iter);
      g_array_remove_index(prefs->conf->desklets, prefs->current);
      
      prefs_enable_form(prefs, FALSE);
      prefs->current = -1;    
      prefs_preview(prefs);
    }
  } else
    dialog_warning(prefs->dlg, _("No desklet selected."));
}

void 
prefs_dialog(GtkWidget * parent, gchar * fullname, 
	     Desklet * desc, gboolean nolock) {
  gint w, h;
  GError * err = NULL;
  GtkWidget * vbox, * hbox, * bbox, * frame,
    * sw, * label, * bt;
  GtkAdjustment * adj;
  EPrefs * prefs;

  if (!(prefs = g_malloc0(sizeof(EPrefs))))
    g_error(_("could not allocate memory"));

  prefs->fullname = fullname;
  prefs->nolock = nolock;
  prefs->conf = config_load(nolock, FALSE);

  if (!prefs->conf->crashmode) {
    prefs->self = config_merge(prefs->conf, desc);
    prefs->last = prefs->conf->desklets->len - 1;

    prefs->dlg = gtk_window_new(GTK_WINDOW_TOPLEVEL);
    gtk_window_set_modal(GTK_WINDOW(prefs->dlg), TRUE);
    gtk_window_set_resizable(GTK_WINDOW(prefs->dlg), FALSE);
    gtk_window_set_title(GTK_WINDOW(prefs->dlg), _("eDesklets Configuration"));
    g_signal_connect(G_OBJECT(prefs->dlg), "destroy",
		     G_CALLBACK(prefs_dialog_free), prefs);

    /* List of desklets */
    prefs_create_listview(prefs);
    misc_set_tooltip(prefs->lv, 
		     _("List of all desklets; \n"
		       "the desklet you configure from\n"
		       "is marked as \"self\"."));
    sw = gtk_scrolled_window_new(NULL, NULL);
    gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(sw), 
				   GTK_POLICY_AUTOMATIC, GTK_POLICY_AUTOMATIC);
    gtk_scrolled_window_add_with_viewport(GTK_SCROLLED_WINDOW(sw), prefs->lv);

    /* Buttons */
    bbox = gtk_vbox_new(FALSE, 0);

    gtk_box_pack_end(GTK_BOX(bbox), 
		     bt = gtk_button_new_from_stock(GTK_STOCK_ADD), 
		     FALSE, FALSE, 5);
    misc_set_tooltip(bt, _("Add a desklet."));
    g_signal_connect(G_OBJECT(bt), "clicked",
		     G_CALLBACK(prefs_dialog_add), prefs);
    
    gtk_box_pack_end(GTK_BOX(bbox), 
		     bt = gtk_button_new_from_stock(GTK_STOCK_DELETE), 
		     FALSE, FALSE, 5);
    misc_set_tooltip(bt, _("Delete selected desklet."));
    g_signal_connect(G_OBJECT(bt), "clicked",
		     G_CALLBACK(prefs_dialog_del), prefs);

    /* Drawing area */
    prefs_get_geometry(&prefs->w, &prefs->h);

    frame = gtk_frame_new(_("Placement Preview"));
    prefs->da = gtk_drawing_area_new();
    gtk_drawing_area_size(GTK_DRAWING_AREA(prefs->da), 200, prefs->h*200/prefs->w);

    misc_set_tooltip(prefs->da, 
		     _("Preview of desklets on screen;\n"
		       "selected desklet is in blue."));
    
    g_signal_connect(G_OBJECT(prefs->da), "expose_event", 
		     G_CALLBACK(prefs_preview_refresh), prefs);

    gtk_container_add(GTK_CONTAINER(frame), prefs->da);
    
    /* Organize them: lv on left, da on right */
    vbox = gtk_vbox_new(FALSE, 10);
    hbox = gtk_hbox_new(FALSE, 10);
    gtk_box_pack_start(GTK_BOX(hbox), sw, TRUE, TRUE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), bbox, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);
    gtk_container_add(GTK_CONTAINER(prefs->dlg), vbox);

    /* Setup URI entry */
    bbox = gtk_vbox_new(FALSE, 0);

    label = gtk_label_new("URI:");
    prefs->uri = gtk_entry_new();
    misc_set_tooltip(prefs->uri, 
		     _("URI to load from;\n"
		       "scheme (e.g. \"http://\") should be specified."));
    g_signal_connect(G_OBJECT(prefs->uri), "changed",
		     G_CALLBACK(prefs_save_form), prefs);

    hbox = gtk_hbox_new(FALSE, 10);
    gtk_box_pack_start(GTK_BOX(hbox), label, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), prefs->uri, TRUE, TRUE, 5);

    gtk_box_pack_start(GTK_BOX(bbox), hbox, FALSE, FALSE, 5);

    /* X, Y, Width, Height entries */
    hbox = gtk_hbox_new(FALSE, 0);

    adj = (GtkAdjustment*)gtk_adjustment_new(50, 0, prefs->w-1, 
					     1, prefs->w/20., 5);
    prefs->x = gtk_spin_button_new(adj, 1.0, 0);
    misc_set_tooltip(prefs->x, _("X position of desklet"));
    g_signal_connect(G_OBJECT(prefs->x), "value-changed",
		     G_CALLBACK(prefs_save_form), prefs);

    gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("X:")), 
		       FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), prefs->x, TRUE, TRUE, 5);
    
    adj = (GtkAdjustment*)gtk_adjustment_new(50, 0, prefs->h-1, 
					     1, prefs->h/20., 5);
    prefs->y = gtk_spin_button_new(adj, 1.0, 0);
    misc_set_tooltip(prefs->y, _("Y position of desklet"));
    g_signal_connect(G_OBJECT(prefs->y), "value-changed",
		     G_CALLBACK(prefs_save_form), prefs);

    gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("Y:")),
		       FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), prefs->y, TRUE, TRUE, 5);

    adj = (GtkAdjustment*)gtk_adjustment_new(50, 1, prefs->w, 
					     1, prefs->w/20., 5);
    prefs->width = gtk_spin_button_new(adj, 1.0, 0);
    misc_set_tooltip(prefs->width, _("Width of desklet"));
    g_signal_connect(G_OBJECT(prefs->width), "value-changed",
		     G_CALLBACK(prefs_save_form), prefs);

    gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("Width:")),
		       FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), prefs->width, TRUE, TRUE, 5);

    adj = (GtkAdjustment*)gtk_adjustment_new(50, 1, prefs->h, 
					     1, prefs->h/20., 5);
    prefs->height = gtk_spin_button_new(adj, 1.0, 0);
    misc_set_tooltip(prefs->height, _("Height of desklet"));
    g_signal_connect(G_OBJECT(prefs->height), "value-changed",
		     G_CALLBACK(prefs_save_form), prefs);

    gtk_box_pack_start(GTK_BOX(hbox), gtk_label_new(_("Height:")),
		       FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(hbox), prefs->height, TRUE, TRUE, 5);

    gtk_box_pack_start(GTK_BOX(bbox), hbox, FALSE, FALSE, 5);

    /* Decorate, timeout and attempts entries */
    hbox = gtk_hbox_new(FALSE, 0);
    
    prefs->decorate = gtk_check_button_new_with_label(_("Decorate"));
    misc_set_tooltip(prefs->decorate, 
		     _("Specify the desklet window should be decorated\n"
		       "(Basically useful when debugging)."));
    g_signal_connect(G_OBJECT(prefs->decorate), "toggled",
		     G_CALLBACK(prefs_save_form), prefs);

    gtk_box_pack_start(GTK_BOX(hbox), prefs->decorate, TRUE, TRUE, 5);

    adj = (GtkAdjustment*)gtk_adjustment_new(0, 0, 10, 1, 2, 5);
    prefs->attempts = gtk_spin_button_new(adj, 1.0, 0);
    g_signal_connect(G_OBJECT(prefs->attempts), "value-changed",
		     G_CALLBACK(prefs_save_form), prefs);
    
    gtk_box_pack_end(GTK_BOX(hbox), prefs->attempts, TRUE, TRUE, 5);
    gtk_box_pack_end(GTK_BOX(hbox), gtk_label_new(_("Attempts:")),
		     FALSE, FALSE, 5);

    misc_set_tooltip(prefs->attempts,
		     _("Number of request attempts;\n"
		       "zero means retrying forever."));
			     
    adj = (GtkAdjustment*)gtk_adjustment_new(0, 0, 60, 1, 10, 5);
    prefs->timeout = gtk_spin_button_new(adj, 1.0, 0);
    misc_set_tooltip(prefs->timeout, 
		     _("Request timeout (in seconds); \n"
		       "zero means waiting indefinitely."));
    g_signal_connect(G_OBJECT(prefs->timeout), "value-changed",
		     G_CALLBACK(prefs_save_form), prefs);

    gtk_box_pack_end(GTK_BOX(hbox), prefs->timeout, TRUE, TRUE, 5);
    gtk_box_pack_end(GTK_BOX(hbox), gtk_label_new(_("Timeout:")),
		     FALSE, FALSE, 5);
    
    gtk_box_pack_start(GTK_BOX(bbox), hbox, FALSE, FALSE, 5);
    
    /* Put the bbox into a frame */
    frame = gtk_frame_new(_("Selected Desklet Settings"));
    gtk_container_add(GTK_CONTAINER(frame), bbox);

    hbox = gtk_hbox_new(FALSE, 0);
    gtk_box_pack_start(GTK_BOX(hbox), frame, FALSE, FALSE, 5);
    gtk_box_pack_start(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

    /* Put buttons down below */
    hbox = gtk_hbox_new(FALSE, 0);
   
    bt = gtk_button_new_from_stock(GTK_STOCK_SAVE);
    misc_set_tooltip(bt, _("Save all changes."));
    g_signal_connect(G_OBJECT(bt), "clicked",
		     G_CALLBACK(prefs_dialog_save), prefs);
    gtk_box_pack_end(GTK_BOX(hbox), bt, FALSE, FALSE, 5);

    bt = gtk_button_new_from_stock(GTK_STOCK_CANCEL); 
    misc_set_tooltip(bt, 
		     _("Cancel all changes\n"
		       "(config will be left unsaved)."));
    g_signal_connect(G_OBJECT(bt), "clicked",
		     G_CALLBACK(prefs_dialog_cancel), prefs);
    gtk_box_pack_end(GTK_BOX(hbox), bt, FALSE, FALSE, 5);

    gtk_box_pack_end(GTK_BOX(vbox), hbox, FALSE, FALSE, 5);

    /* Show time! */
    gtk_widget_show_all(prefs->dlg);
    
  } else {
    dialog_error(parent, prefs->conf->crashmsg);
    g_free(prefs);
  }
}
